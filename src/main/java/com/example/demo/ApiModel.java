package com.example.demo;


import dev.langchain4j.model.openai.OpenAiChatModel;

public class ApiModel {
    private String apiKey;
    private OpenAiChatModel model;

    public ApiModel() {
        apiKey = "demo";
        model = OpenAiChatModel.withApiKey(apiKey);
    }

    public OpenAiChatModel getModel() {
        return this.model;
    }
}
