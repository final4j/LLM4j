# LLM4j

Using [LangChain4j](https://github.com/langchain4j/langchain4j) to communicate with LLMs in SpringBoot.

### Import Dependency
For Maven in `pom.xml`: 
```xml
<dependency>
    <groupId>dev.langchain4j</groupId>
    <artifactId>langchain4j-open-ai</artifactId>
    <version>0.29.1</version>
</dependency>
```


### Architecture Pattern
```
View(display reponse) <-- Controller(interface) <--> Model(set up LLM)
```