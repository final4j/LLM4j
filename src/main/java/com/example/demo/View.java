package com.example.demo;

public class View {
    ApiController controller;

    private View() {
        controller = ApiController.getInstance();
    }

    private static final View instance = new View();

    public static View getInstance() {
        return instance;
    }

    public void display() {
        String msg = "Say Hi";
        String ans = controller.generate(msg);
        System.out.println(ans);
    }
}
