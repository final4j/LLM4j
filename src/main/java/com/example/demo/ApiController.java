package com.example.demo;

import dev.langchain4j.model.openai.OpenAiChatModel;

public class ApiController {
    private ApiModel apiModel;
    private OpenAiChatModel model;

    private ApiController() {
        apiModel = new ApiModel();
        model = apiModel.getModel();
    }

    private final static ApiController instance = new ApiController();

    public static ApiController getInstance() {
        return instance;
    }

    public String generate(String message) {
        String response = model.generate(message);
        return response;
    }
}
